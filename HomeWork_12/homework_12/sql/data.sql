-- filling in the driver table
insert into driver (first_name, last_name, phone_number, experience, age, drivers_license,
                    license_categories, rating)
values ('Bulat', 'Isanbirdin', '89995553322', 6, 24, true, 'B', 4.5);
insert into driver (first_name, last_name, phone_number, experience, age, drivers_license,
                    license_categories, rating)
values ('Azamat', 'Galiev', '87863541363', 7, 25, true, 'B', 5);
insert into driver (first_name, last_name, phone_number, age, drivers_license)
values ('Ruslan', 'Shaimardanov', '89653253645', 24, true);

update driver
set experience=6,
    license_categories='A, B, C, D, M',
    rating=4.8
where id = 3;

insert into driver (first_name, last_name, phone_number, age, drivers_license)
values ('Finat', 'Sitdikov', 89657899878, 24, false);

insert into driver (first_name, last_name, phone_number, age, drivers_license)
values ('Muchametdinov', 'Linar', '89654785698', 23, false),
       ('Ponedelnikov', 'Nikita', '85236547896', 24, true);

update driver
set experience=3,
    license_categories='B',
    rating=3.3
where id = 6;

-- filling in the car table
insert into car (model, car_number, id_owner)
values ('Camry', 'o001oo702', 25),
       ('Highlander', 'm125pa016', 15),
       ('X6', 'a125aa102', 13),
       ('Granta', 'p555pp716', 15),
       ('Priora', 'y256yo102', 20),
       ('Aveo', 'o556oo118', 13);

-- filling in the trip table
insert into trip (id_driver, id_car, trip_date, duration_trip_days)
values (1, 2, '2022-01-11', 20),
       (2, 4, '2022-11-07', 1),
       (3, 1, '2022-08-20', 10),
       (4, 6, '2021-11-20', 6),
       (5, 5, '2020-05-05', 30),
       (6, 4, '2021-02-15', 15);
