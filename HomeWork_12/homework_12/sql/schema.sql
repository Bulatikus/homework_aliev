-- deleting tables
drop table if exists trip;
drop table if exists driver;
drop table if exists car;

-- creating driver table
create table driver
(
    id                 bigserial primary key,
    first_name         char(20),
    last_name          char(20),
    phone_number       char(11) unique,
    experience         integer          default 0,
    age                integer check ( age >= 0 and age <= 120 )                          not null,
    drivers_license    bool,
    license_categories char(20)         default 'no drivers license',
    rating             double precision default 0.0 check ( rating >= 0 and rating <= 5 ) not null
);

-- creating car table
create table car
(
    id         bigserial primary key,
    model      char(20)       not null,
    car_number char(9) unique not null,
    id_owner   integer        not null
);

-- creating trip table
create table trip
(
    id                 bigserial primary key,
    id_driver          bigint  not null,
    foreign key (id_driver) references driver (id),
    id_car             bigint  not null,
    foreign key (id_car) references car (id),
    trip_date          timestamp,
    duration_trip_days integer not null
);
