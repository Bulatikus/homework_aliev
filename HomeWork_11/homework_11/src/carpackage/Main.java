package carpackage;

public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("resources/Car.txt");
        System.out.println("\nWorking of the 'getNumber' method: " + carsRepository.getNumber());
        System.out.println("\nWorking of the 'uniqueModel' method: " + carsRepository.uniqueModel());
        System.out.println("\nWorking of the 'getColorMinPrice' method: " + carsRepository.getColorMinPrice());
        System.out.println("\nWorking of the 'averagePrice' method: " + carsRepository.averagePrice());
    }
}