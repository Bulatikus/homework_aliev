package carpackage;

public interface CarsRepository {

    String getNumber();

    int uniqueModel();

    String getColorMinPrice();

    double averagePrice();
}
