package carpackage;

public class UnsuccessfulWorkWithFileException extends RuntimeException {
    public UnsuccessfulWorkWithFileException(Throwable e) {
        super(e);
    }
}
