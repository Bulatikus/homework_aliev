package productrepository;

import java.io.*;
import java.util.List;
import java.util.function.Function;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {

    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double price = Double.parseDouble(parts[2]);
        Integer quantity = Integer.parseInt(parts[3]);
        return new Product(id, name, price, quantity);
    };

    private static final Function<Product, String> productToStringMapper = product ->
            product.getId() + "|" + product.getName() + "|" + product.getPrice() + "|" + product.getQuantity();

    @Override
    public Product findById(Integer id) {
        try (FileReader fileReader = new FileReader(fileName);
             BufferedReader reader = new BufferedReader(fileReader)) {
            return reader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId().equals(id))
                    .findAny().orElse(null);
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (FileReader fileReader = new FileReader(fileName);
             BufferedReader reader = new BufferedReader(fileReader)) {
            return reader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getName().contains(title)).toList();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public void update(Product product) {
        try (FileReader fileReader = new FileReader(fileName);
             BufferedReader reader = new BufferedReader(fileReader)) {
            List<Product> products = reader
                    .lines()
                    .map(stringToProductMapper).toList();
            for (Product updateProduct : products) {
                if (updateProduct.getId().equals(product.getId())) {
                    updateProduct.setPrice(product.getPrice());
                    updateProduct.setQuantity(product.getQuantity());
                    writeToFile(products);
                }
            }
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    private void writeToFile(List<Product> products) {
        try (FileWriter fileWriter = new FileWriter(fileName);
             BufferedWriter writer = new BufferedWriter(fileWriter)) {
            for (Product current : products) {
                String productToSave = productToStringMapper.apply(current);
                writer.write(productToSave);
                writer.newLine();
            }
        } catch (IOException e) {
            throw new UnsuccessfulWriteToFileException(e);
        }
    }
}
