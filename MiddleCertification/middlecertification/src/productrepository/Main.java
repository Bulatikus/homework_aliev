package productrepository;

public class Main {
    public static void main(String[] args) {
        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("resources/Product.txt");

        System.out.println("\nWorking of the 'findById' method: " + productsRepository.findById(1));
        System.out.println("\nWorking of the 'findAllByTitleLike' method: " + productsRepository.findAllByTitleLike("ок"));

        Product milk = productsRepository.findById(1);
        milk.setPrice(40.1);
        milk.setQuantity(10);
        productsRepository.update(milk);
        System.out.println("\nTo see how the 'update 'method works, open the 'product_repository.Product.txt' file.");
    }
}