package productrepository;

public class UnsuccessfulWriteToFileException extends RuntimeException {
    public UnsuccessfulWriteToFileException(Throwable e) {
        super(e);
    }
}
