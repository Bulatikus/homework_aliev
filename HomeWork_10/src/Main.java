import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        //Создание Map
        Map<String, Integer> wordMap = new HashMap<>();
        //Передаваемая строка (как из примера)
        String line = "Hello Hello bye Hello bye Inno";
        String[] strings = line.split(" ");
        //Заполнение Map
        for (String word : strings) {
            Integer count = wordMap.get(word);
            if (count == null) {
                wordMap.put(word, 1);
            } else {
                wordMap.put(word, count + 1);
            }
        }
        //Присваивание значений key и value первой пары Map
        Map.Entry<String, Integer> entry = wordMap.entrySet().iterator().next();
        String key = entry.getKey();
        Integer value = entry.getValue();
        //Прохождение всей Map в поисках максимального value
        for (Map.Entry<String, Integer> entrys : wordMap.entrySet()) {
            if (entrys.getValue() > value) {
                key = entrys.getKey();
                value = entrys.getValue();
            }
        }
        //Вывод ответа
        System.out.println(key + " " + value);
    }
}
