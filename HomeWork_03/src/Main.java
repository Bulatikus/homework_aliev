import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int count = 0;

        System.out.println("Введите размер массива, массив должен быть > 0");

        int arraySize = scanner.nextInt();

        int[] array = new int[arraySize];

        if (array.length == 0) {
            System.out.println("В массиве 0 элементов. Невозможно посчитать количество локальных минимумов.");
        } else if (array.length > 0) {
            System.out.println("Введите элементы массива");
            for (int i = 0; i < arraySize; i++) {
                array[i] = scanner.nextInt();
            }
        }

        if (array.length > 1) {
            for (int i = 1; i < arraySize - 1; i++) {
                if (array[i] < array[i - 1] && array[i] < array[i + 1]) {
                    count++;
                }
            }
            if (array[0] < array[1]) {
                count++;
            }
            if (array[arraySize - 1] < array[arraySize - 2]) {
                count++;
            }
            System.out.println("Количество локальных минимумов: " + count);
        } else if (array.length == 1) {
            System.out.println("Количество локальных минимумов: 1");
        }
    }
}
