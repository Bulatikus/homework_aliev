import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //Rectangle
        System.out.println("Please, enter the X, Y coordinates and height and width for the rectangle");
        Rectangle rectangle = new Rectangle(scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
        System.out.println("Rectangle with sides: " + rectangle.getHeight() + " and " + rectangle.getWidth());
        System.out.println("The center of the rectangle: X = " + rectangle.getCentreX() + ", Y = " + rectangle.getCentreY());
        System.out.println("Rectangle's perimeter: " + rectangle.calcPerimeter());
        System.out.println("Rectangle's area: " + rectangle.calcSquare());
        System.out.println("Please,set the new center of the rectangle");
        rectangle.move(scanner.nextInt(), scanner.nextInt());

        //Square
        System.out.println("Please enter the X, Y coordinates and side for the square");
        Square square = new Square(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
        System.out.println("Square with side: " + square.getSizeLength());
        System.out.println("The center of the square: X = " + square.getCentreX() + ", Y = " + square.getCentreY());
        System.out.println("Square's perimeter: " + square.calcPerimeter());
        System.out.println("Square's area: " + square.calcSquare());
        System.out.println("Please,set the new center of the square");
        square.move(scanner.nextInt(), scanner.nextInt());

        //Circle
        System.out.println("Please, enter the X, Y coordinates and radius for the circle");
        Circle circle = new Circle(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
        System.out.println("Circle with radius: " + circle.getRadius());
        System.out.println("The center of the circle: X = " + circle.getCentreX() + ", Y = " + circle.getCentreY());
        System.out.println("Circle's perimeter: " + circle.calcPerimeter());
        System.out.println("Circle's area: " + circle.calcSquare());
        System.out.println("Please,set the new center of the circle");
        circle.move(scanner.nextInt(), scanner.nextInt());

        //Ellipse
        System.out.println("Please, enter X, Y coordinates and small and big radius for the ellipse");
        Ellipse ellipse = new Ellipse(scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
        System.out.println("Ellipse with radii: " + ellipse.getSmallRadius() + " and " + ellipse.getBigRadius());
        System.out.println("The center of the ellipse: X = " + ellipse.getCentreX() + ", Y = " + ellipse.getCentreY());
        System.out.println("Ellipse's perimeter: " + ellipse.calcPerimeter());
        System.out.println("Ellipse's area: " + ellipse.calcSquare());
        System.out.println("Please,set the new center of the ellipse");
        ellipse.move(scanner.nextInt(), scanner.nextInt());
    }
}
