public class Ellipse extends Shape {
    private int smallRadius;
    private int bigRadius;

    public Ellipse(int x, int y, int sR, int bR) {
        super(x, y);
        smallRadius = sR;
        bigRadius = bR;
    }

    public int getSmallRadius() {
        return smallRadius;
    }

    public int getBigRadius() {
        return bigRadius;
    }

    public double calcPerimeter() {
        return 2 * Math.PI * Math.sqrt((smallRadius * smallRadius + bigRadius * bigRadius) / 2);
    }

    public double calcSquare() {
        return Math.PI * smallRadius * bigRadius;
    }
}
