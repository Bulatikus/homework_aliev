public class Shape {
    private int centreX;
    private int centreY;

    public Shape(int x, int y) {
        centreX = x;
        centreY = y;
    }

    public int getCentreX() {
        return centreX;
    }

    public int getCentreY() {
        return centreY;
    }

    public void move(int toX, int toY) {
        if (toX == centreX && toY == centreY) {
            System.out.println("You didn't move the shape\n");
        } else {
            System.out.println("You moved the shape. New coordinates of the shape: X = " + toX + ", Y = " + toY + "\n");
        }
    }
}
