public class Circle extends Shape {
    private int radius;

    public Circle(int x, int y, int r) {
        super(x, y);
        radius = r;
    }

    public int getRadius() {
        return radius;
    }

    public double calcPerimeter() {
        return 2 * Math.PI * radius;
    }

    public double calcSquare() {
        return Math.PI * radius * radius;
    }
}
