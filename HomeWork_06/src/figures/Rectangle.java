public class Rectangle extends Shape {
    private int height;
    private int width;

    public Rectangle(int x, int y, int h, int w) {
        super(x, y);
        height = h;
        width = w;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int calcPerimeter() {
        return 2 * (height + width);
    }

    public int calcSquare() {
        return height * width;
    }
}
