public class Square extends Shape {
    private int sizeLength;

    public Square(int x, int y, int s) {
        super(x, y);
        sizeLength = s;
    }

    public int getSizeLength() {
        return sizeLength;
    }

    public int calcPerimeter() {
        return 4 * sizeLength;
    }

    public int calcSquare() {
        return sizeLength * sizeLength;
    }
}
