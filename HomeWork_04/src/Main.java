import javax.imageio.ImageTranscoder;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // Task 1
        System.out.println("Введите значение левой границы");
        int leftNumber = scanner.nextInt();
        System.out.println("Введите значение правой границы");
        int rightNumber = scanner.nextInt();
        System.out.println("Использование функции calcSum: " + calcSum(leftNumber, rightNumber));

        System.out.println();
        // Task 2
        try {
            int[] arrayTwoTask = newArray(scanner);
            if (arrayTwoTask.length > 0) {
                outputOfEvenElements(arrayTwoTask);
            } else if (arrayTwoTask.length == 0) {
                System.err.println("В массиве нет чисел");
            }
        } catch (NegativeArraySizeException e) {
            System.err.println("Длина массива не может быть отрицательной величиной");
        }


        System.out.println("\n");
        // Task 3
        int[] arrayThreeTask = newArray(scanner);
        System.out.println("Перевод массива в строку: " + toInt(arrayThreeTask));
    }

    // Creating an array for task 2 and task 3
    private static int[] newArray(Scanner scanner) {
        System.out.println("Введите длину массива");
        int arrayLength = scanner.nextInt();

        int[] arrayInput = new int[arrayLength];

        System.out.println("Введите значения массива");
        for (int i = 0; i < arrayInput.length; i++) {
            arrayInput[i] = scanner.nextInt();
        }
        return arrayInput;
    }

    // Task 1
    public static int calcSum(int from, int to) {
        int answer = 0;

        if (from < to) {
            for (int i = from; i <= to; i++) {
                answer += i;
            }
        } else if (from == to) {
            answer = from;
        } else {
            answer = -1;
        }
        return answer;
    }

    // Task 2
    public static void outputOfEvenElements(int[] array) {
        System.out.print("Четные значения массива: ");
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                System.out.print(array[i] + " ");
            }
        }
    }

    // Task 3
    public static int toInt(int[] array) {
        int number = 0;
        for (int i = array.length - 1, k = 0; i >= 0; i--, k++) {
            number += array[i] * Math.pow(10, k);
        }
        return number;
    }
}
