package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.CourseForm;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.models.User;
import ru.inno.ec.repositories.CoursesRepository;
import ru.inno.ec.repositories.LessonsRepository;
import ru.inno.ec.repositories.UsersRepository;
import ru.inno.ec.services.CoursesService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CoursesServiceImpl implements CoursesService {

    private final CoursesRepository coursesRepository;
    private final UsersRepository usersRepository;
    private final LessonsRepository lessonsRepository;

    @Override
    public void addStudentToCourse(Long courseId, Long studentId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        User student = usersRepository.findById(studentId).orElseThrow();

        student.getCourses().add(course);

        usersRepository.save(student);
    }

    @Override
    public Course getCourse(Long courseId) {
        return coursesRepository.findById(courseId).orElseThrow();
    }

    @Override
    public List<User> getNotInCourseStudents(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesNotContains(course);
    }

    @Override
    public List<User> getInCourseStudents(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesContains(course);
    }

    @Override
    public List<Course> getAllCourses() {
        return coursesRepository.findAllByStateNot(Course.State.DELETED);
    }

    @Override
    public void addCourse(CourseForm course) {
        Course newCourse = Course.builder()
                .title(course.getTitle())
                .description(course.getDescription())
                .state(Course.State.CONFIRMED)
                .build();

        coursesRepository.save(newCourse);
    }

    @Override
    public void deleteCourse(Long courseId) {
        Course courseForDelete = coursesRepository.findById(courseId).orElseThrow();
        courseForDelete.setState(Course.State.DELETED);

        coursesRepository.save(courseForDelete);
    }

    @Override
    public List<Lesson> getInCourseLessons(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return lessonsRepository.findLessonsByCourse(course);
    }

    @Override
    public void addLessonToCourse(Long courseId, Long lessonId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        Lesson lesson = lessonsRepository.findById(lessonId).orElseThrow();

        lesson.setCourse(course);

        lessonsRepository.save(lesson);
    }

    @Override
    public void updateCourse(Long courseId, CourseForm courseForm) {
        Course courseForUpdate = coursesRepository.findById(courseId).orElseThrow();
        courseForUpdate.setTitle(courseForm.getTitle());
        courseForUpdate.setDescription(courseForm.getDescription());

        coursesRepository.save(courseForUpdate);
    }

    @Override
    public List<Lesson> getNotInCourseLessons() {
        return lessonsRepository.findLessonsByCourseIsNull();
    }
}
