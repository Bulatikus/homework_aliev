package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.LessonForm;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.repositories.LessonsRepository;
import ru.inno.ec.services.LessonsService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;

    @Override
    public List<Lesson> getAllLessons() {
        return lessonsRepository.findAllByStateNot(Lesson.State.DELETED);
    }

    @Override
    public void addLesson(LessonForm lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .summary(lesson.getSummary())
                .state(Lesson.State.CONFIRMED)
                .build();

        lessonsRepository.save(newLesson);
    }

    @Override
    public void deleteLesson(Long lessonId) {
        Lesson lessonForDelete = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForDelete.setState(Lesson.State.DELETED);

        lessonsRepository.save(lessonForDelete);
    }

    @Override
    public Lesson getLesson(Long id) {
        return lessonsRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateLesson(Long lessonId, LessonForm lessonForm) {
        Lesson lessonForUpdate = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForUpdate.setName(lessonForm.getName());
        lessonForUpdate.setSummary(lessonForm.getSummary());

        lessonsRepository.save(lessonForUpdate);
    }
}
