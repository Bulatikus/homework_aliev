public class OddNumbersPrintTask extends AbstractNumbersPrintTask {

    public OddNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    @Override
    public void complete() {
        if (from < to) {
            for (int i = from; i <= to; i++) {
                if (i % 2 != 0) {
                    System.out.print(i + " ");
                }
            }
            System.out.println();
        } else {
            System.out.println("Диапазон был задан неверно");
        }
    }
}
