import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество массивов");
        int arraysNumber = scanner.nextInt();
        Task[] tasks = new Task[arraysNumber];

        int count = 1;

        for (int i = 0; i < tasks.length; i++) {
            System.out.println("Для вывода четных чисел массива нажмите - 1, нечетных - 2");
            int choice = scanner.nextInt();
            if (choice == 1) {
                System.out.println("Введите левую и правую границы " + count + " массива (левая < правой)");
                int leftBoard = scanner.nextInt();
                int rightBoard = scanner.nextInt();
                tasks[i] = new EvenNumbersPrintTask(leftBoard, rightBoard);
                count++;
            } else if (choice == 2) {
                System.out.println("Введите левую и правую границы " + count + " массива (левая < правой)");
                int leftBoard = scanner.nextInt();
                int rightBoard = scanner.nextInt();
                tasks[i] = new OddNumbersPrintTask(leftBoard, rightBoard);
                count++;
            }
        }
        completeAllTasks(tasks);
    }

    public static void completeAllTasks(Task[] tasks) {
        for (int i = 0; i < tasks.length; i++) {
            System.out.print("Запрошенные числа для " + (i + 1) + " массива: ");
            tasks[i].complete();
        }
    }
}
