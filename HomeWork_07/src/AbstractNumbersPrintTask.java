public abstract class AbstractNumbersPrintTask implements Task {
    int from;
    int to;

    public AbstractNumbersPrintTask(int from, int to) {
        if (from < to) {
            this.from = from;
            this.to = to;
        } else {
            System.out.println("Диапазон задан неверно. В массиве не будет чисел.");
        }
    }

    @Override
    public abstract void complete();
}
