import java.util.Scanner;

public class Main {
    private static final int ARRAY_COUNT = 6;

    public static void main(String[] args) {
        //Создание массива фиксированной длины ARRAY_COUNT
        int[] startArray = new int[ARRAY_COUNT];

        Scanner scanner = new Scanner(System.in);
        //Заполнение массива
        System.out.println("Введите " + ARRAY_COUNT + " целочисленных элементов массива");
        for (int i = 0; i < startArray.length; i++) {
            startArray[i] = scanner.nextInt();
        }

        //Сумма элементов массива в промежутке от from до to
        ArrayTask sumElement = (array, from, to) -> {
            int sum = 0;
            //Если from<=to, происходит суммирование
            if (from <= to) {
                for (int i = from; i <= to; i++) {
                    sum += array[i];
                }
                //Если from>to, происходит замена from на to, to на from, и происходит суммирование
            } else {
                int temp = from;
                from = to;
                to = temp;
                for (int i = from; i <= to; i++) {
                    sum += array[i];
                }
                System.out.println("Ошибочно заданы границы массива. Возможно вы имели ввиду от " + (from + 1) + " до "
                        + (to + 1) + ". Возможные результаты для вашего ввода:");
            }
            System.out.print("Сумма элементов массива в промежутке от " + (from + 1) + " до " + (to + 1) + ": ");
            return sum;
        };

        //Сумма цифр элемента с максимальным значением в промежутке от from до to
        ArrayTask sumMaxElement = (array, from, to) -> {
            int max = array[from];
            //Если from<=to, выполняется действие
            if (from <= to) {
                for (int i = from; i <= to; i++) {
                    if (array[i] > max) {
                        max = array[i];
                    }
                }
                //Если from>to, происходит замена from на to, to на from
            } else {
                int temp = from;
                from = to;
                to = temp;
                for (int i = from; i <= to; i++) {
                    if (array[i] > max) {
                        max = array[i];
                    }
                }
                System.out.println("Ошибочно заданы границы массива. Возможно вы имели ввиду от " + (from + 1) + " до "
                        + (to + 1) + ". Возможные результаты для вашего ввода:");
            }
            int sum = 0;
            while (max != 0) {
                sum += max % 10;
                max /= 10;
            }
            System.out.print("Сумма цифр элемента с максимальным значением в промежутке от " + (from + 1) + " до "
                    + (to + 1) + ": ");
            return sum;
        };

        //Использование лямбда-выражений
        ArraysTasksResolver arraysTasksResolver = new ArraysTasksResolver();
        int from = 0;
        int to = 0;
        System.out.println("Введите левую и правую границы массива, в пределах от 1 до " + ARRAY_COUNT +
                " Левая граница должна быть меньше правой");
        try {
            from = scanner.nextInt();
            to = scanner.nextInt();
            if ((from - 1) > 0 || (from - 1) < (ARRAY_COUNT - 1) || (to - 1) < (ARRAY_COUNT - 1) || (to - 1) > 0) {
                arraysTasksResolver.resolveTask(startArray, sumElement, (from - 1), (to - 1));
                arraysTasksResolver.resolveTask(startArray, sumMaxElement, (from - 1), (to - 1));
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("\nВы вышли за границы массива. Дальше программа не сможет работать");
        }
    }
}
