public class ArraysTasksResolver {
    public static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        //Вывод исходного массива
        System.out.print("\nИсходный массив: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        //Вывод значений массива на позициях from и to
        System.out.println("\nЗначение массива на " + (from + 1) + " позиции: " + array[from]);
        System.out.println("Значение массива на " + (to + 1) + " позиции: " + array[to]);

        //Реализация лямбда-выражений
        int result = task.resolve(array, from, to);
        System.out.println(result);
    }
}
