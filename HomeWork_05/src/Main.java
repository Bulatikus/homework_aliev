import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean go = true;

        CashMachine cashMachine = new CashMachine(3000, 4000, 10000);
        System.out.println("Банкомат приветствует Вас!");

        while (go) {
            System.out.println("\nВыберите операцию: 1 - выввести денег; 2 - положить деньги; 0 - прекратить работу банкомата");
            int number = scanner.nextInt();
            if (number == 0) {
                System.out.println("Работа банкомата прекращена");
                go = false;
            }
            if (number == 1) {
                System.out.println("Сейчас в банкомате: " + cashMachine.getSumCashMachineMoney());
                System.out.println("Максимальная сумма для выдачи: " + cashMachine.getMaxGiveMoney());
                System.out.println("\nВведите сумму, которую Вы хотите получить");
                cashMachine.giveMoney(scanner.nextInt());
            } else if (number == 2) {
                System.out.println("Сейчас в банкомате: " + cashMachine.getSumCashMachineMoney());
                System.out.println("Максимальная сумма для выдачи: " + cashMachine.getMaxGiveMoney());
                System.out.println("\nВведите сумму, которую Вы хотите положить");
                cashMachine.depositMoney(scanner.nextInt());
            }
        }
    }
}
