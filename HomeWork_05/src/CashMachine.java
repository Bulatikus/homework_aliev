public class CashMachine {
    private int sumCashMachineMoney;    //Сумма оставшихся денег в банкомате
    private int maxGiveMoney;           //Максимальная сумма для выдачи
    private int maxCashMachineMoney;    //Максимальное количество денег в банкомате
    private int countOperations;        //Количество проведенных операций

    public CashMachine(int sumCashMachineMoney, int maxGiveMoney, int maxCashMachineMoney) {
        this.sumCashMachineMoney = sumCashMachineMoney;
        this.maxGiveMoney = maxGiveMoney;
        this.maxCashMachineMoney = maxCashMachineMoney;
    }

    public void giveMoney(int money) {
        if (money > maxGiveMoney) {
            System.out.println("Банкомат не может выдать Вам сумму больше " + maxGiveMoney + ". Вам выдано: " + maxGiveMoney);
            this.sumCashMachineMoney -= maxGiveMoney;
        } else if (money > sumCashMachineMoney) {
            System.out.println("В банкомате нет запрошенной Вами суммы денег. Вам выдано: " + sumCashMachineMoney);
            sumCashMachineMoney -= sumCashMachineMoney;
        } else {
            System.out.println("Вам выдано: " + money);
            sumCashMachineMoney -= money;
        }
        countOperations++;
        System.out.println("Операций с банкоматом проведено: " + countOperations);
    }

    public void depositMoney(int money) {
        if ((money + sumCashMachineMoney) > maxCashMachineMoney) {
            System.out.println("Банкомат переполнен. Возврат суммы, которую Вы не смогли положить: " +
                    ((money + sumCashMachineMoney) - maxCashMachineMoney));
            sumCashMachineMoney = maxCashMachineMoney;
        } else {
            System.out.println("Вы положили на счет: " + money);
            sumCashMachineMoney += money;
        }
        countOperations++;
        System.out.println("Операций с банкоматом проведено: " + countOperations);
    }

    public int getSumCashMachineMoney() {
        return sumCashMachineMoney;
    }

    public int getMaxGiveMoney() {
        return maxGiveMoney;
    }
}
