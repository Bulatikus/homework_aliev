package ru.inno;

public class LinkedList<T> implements List<T> {

    private Node<T> first;

    private Node<T> last;
    private int count;

    private static class Node<E> {
        E value;
        Node<E> next;

        public Node(E value) {
            this.value = value;
        }
    }

//    @Override
//    public void add(T element) {
//        // создаем новый узел со значением
//        Node<T> newNode = new Node<>(element);
//        // если элементов в списке нет
//        if (count == 0) {
//            // кладем новый узел в качестве первого
//            this.first = newNode;
//        } else {
//            // если в списке уже есть узлы, нужно добраться до последнего
//
//            // запоминаем ссылку на первый узел
//            Node<T> current = this.first;
//            // пока не долшли до узла, у которого нет следующего узла (до последнего)
//            while (current.next != null) {
//                current = current.next;
//            }
//            // теперь у нас current указывает на последний узел
//
//            // делаем следующим после последнего - новый узел
//            current.next = newNode;
//        }
//        count++;
//    }

    @Override
    public void add(T element) {
        // создаем новый узел со значением
        Node<T> newNode = new Node<>(element);
        // если элементов в списке нет
        if (count == 0) {
            // кладем новый узел в качестве первого и последнего
            this.first = newNode;
        } else {
            // если в списке уже есть узлы
            // у последнего делаем следующим новый узел
            this.last.next = newNode;
        }
        this.last = newNode;
        count++;
    }

    //homework_09 - 1 дополнительное задание
    @Override
    public void remove(T element) {
        Node<T> prev = null;
        Node<T> current = this.first;
        while (current != null) {
            if (current.value.equals(element)) {
                if (prev != null) {
                    prev.next = current.next;
                } else {
                    this.first = this.first.next;
                }
                count--;
                break;
            }
            prev = current;
            current = current.next;
        }
    }

    //homework_09 - 2 дополнительное задание
    @Override
    public void removeAt(int index) {
        Node<T> prev = null;
        Node<T> current = this.first;
        int currentIndex = 0;
        while (current != null) {
            if (currentIndex == index) {
                if (prev != null) {
                    prev.next = current.next;
                } else {
                    this.first = this.first.next;
                }
                count--;
                break;
            }
            currentIndex++;
            prev = current;
            current = current.next;
        }
    }

    @Override
    public boolean contains(T element) {
        // получить ссылку на первый элемент списка
        // пройтись по всем элементам списка
        Node<T> current = this.first;
        // пока не обошли весь список
        while (current != null) {
            // если значение в текущем узле совпало с искомым
            if (current.value.equals(element)) {
                return true;
            }
            // если не совпало - идем к следующему узлу
            current = current.next;
        }
        // если не нашли элемент
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public T get(int index) {
        if (0 <= index && index <= count) {
            // начинаем с первого элемента
            Node<T> current = this.first;

            // если запрошенный индекс был равен - 3
            // 0, 1, 2
            for (int i = 0; i < index; i++) {
                // на каждом шаге цикла двигаемся дальше
                current = current.next;
            }
            return current.value;
        }
        return null;

    }
}

