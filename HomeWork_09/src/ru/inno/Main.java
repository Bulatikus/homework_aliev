package ru.inno;

import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
//        List<String> stringList = new LinkedList<>();
//
//        stringList.add("Hello!");
//        stringList.add("Bye!");
//        stringList.add("Fine!");
//        stringList.add("Bye!");
//        stringList.add("PHP!");
//        stringList.add("Cobol!");
//
//        System.out.println("До remove");
//        for (int i=0;i<stringList.size();i++){
//            System.out.println(stringList.get(i));
//        }
//        System.out.println("\n");
//        stringList.removeAt(3);
//        System.out.println("После remove");
//        for (int i=0;i<stringList.size();i++){
//            System.out.println(stringList.get(i));
//        }
//
//        System.out.println(stringList.get(0));
//        System.out.println(stringList.get(3));
//        System.out.println(stringList.get(5));
//        System.out.println(stringList.contains("Fine!"));
//        System.out.println(stringList.contains("Cobol!"));
//        System.out.println(stringList.contains("Python"));
//        System.out.println(stringList.size());

        //homework_09

        /**
         * Проверка remove для ArrayList
         */
        //Вывод начального ArrayList
        List<Integer> integerArrayListRemove = getArrayList();
        //Вызов метода remove
        integerArrayListRemove.remove(11);
        //Вывод нового ArrayList
        getNewArrayList(integerArrayListRemove, "ArrayList после remove");

        System.out.println("\n");

        /**
         * Проверка removeAt для ArrayList
         */
        //Вывод начального ArrayList
        List<Integer> integerArrayListRemoveAt = getArrayList();
        //Вызов метода removeAt
        integerArrayListRemoveAt.removeAt(4);
        //Вывод нового ArrayList
        getNewArrayList(integerArrayListRemoveAt, "ArrayList после removeAt");

        System.out.println("\n");

        /**
         * Проверка remove для LinkedList
         */
        //Вывод начального LinkedList
        List<Integer> integerLinkedListRemove = getLinkedList();
        //Вызов метода remove
        integerLinkedListRemove.remove(11);
        //Вывод нового LinkedList
        getNewArrayList(integerLinkedListRemove, "LinkedList после remove");

        System.out.println("\n");

        /**
         * Проверка removeAt для LinkedList
         */
        //Вывод начального LinkedList
        List<Integer> integerLinkedListRemoveAt = getLinkedList();
        //Вызов метода remove
        integerLinkedListRemoveAt.removeAt(4);
        //Вывод нового LinkedList
        getNewArrayList(integerLinkedListRemoveAt, "LinkedList после removeAt");
    }

    //Создание ArrayList в виде метода (для удобства)
    private static List<Integer> getArrayList() {
        List<Integer> integerList = new ArrayList<>();

        integerList.add(8);
        integerList.add(10);
        integerList.add(11);
        integerList.add(13);
        integerList.add(11);
        integerList.add(15);
        integerList.add(-5);
        System.out.println("Изначальный ArrayList");
        for (int i = 0; i < integerList.size(); i++) {
            System.out.print(integerList.get(i) + " ");
        }
        System.out.println();
        return integerList;
    }

    //Создание LinkedList в виде метода (для удобства)
    private static List<Integer> getLinkedList() {
        List<Integer> integerList = new LinkedList<>();

        integerList.add(8);
        integerList.add(10);
        integerList.add(11);
        integerList.add(13);
        integerList.add(11);
        integerList.add(15);
        integerList.add(-5);
        System.out.println("Изначальный LinkedList");
        for (int i = 0; i < integerList.size(); i++) {
            System.out.print(integerList.get(i) + " ");
        }
        System.out.println();
        return integerList;
    }

    //Вывод нового ArrayList и LinkedList в виде метода (для удобства)
    private static void getNewArrayList(List<Integer> integerList, String s) {
        System.out.println(s);
        for (int i = 0; i < integerList.size(); i++) {
            System.out.print(integerList.get(i) + " ");
        }
    }
}

